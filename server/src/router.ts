import express from 'express';
import child_process from 'child_process'; // part of Node.js as module providing to product subproecesses

const router = express.Router();

router.post('/test', (request, response) => {
  console.log('response', response);

  if (
    request.body &&
    typeof request.body.language == 'string' &&
    typeof request.body.value == 'string'
  ) {
    let output = '';
    let error = '';

    // child_process.spawn(command[, args][, options])
    const process = child_process.spawn('docker', [
      'run',
      '--rm',
      'node',
      'node',
      '-e',
      request.body.value,
    ]);
    process.stdout.on('data', (data) => {
      output += data;
    });
    process.stderr.on('data', (data) => {
      error += data;
    });
    process.on('close', (exitStatus: number) => {
      return response.send({ exitStatus: exitStatus, stdout: output, stderr: error });
    });
  } else response.status(400).send('Missing properties');
});

export default router;

// Command for docker in terminal
// 1. docker search [image_name] : show if the image exist
// 2. docker pull [image_name]: download the image from docker-hub and save it in lacal
// 3. docker run --name [container_name] [image_name] : build container based on the image
