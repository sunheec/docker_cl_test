import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { Form, Card, Button } from './widgets';
import axios from 'axios';
// type Data = {
//   exitStatus: string;
//   output: string;
//   error: string;
// };

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

class Home extends Component {
  value = '';
  exitStatus = '';
  output = '';
  error = '';

  handleOnChange = (event: React.ChangeEvent<any>) => {
    this.value = event.currentTarget.value;
    console.log(event.currentTarget.value);
  };

  handleOnClick = () => {
    axios
      .post('/test', { language: 'js', value: this.value })
      .then((response) => {
        // console.log(response);
        this.exitStatus = response.data.exitStatus;
        this.output = response.data.stdout;
        this.error = response.data.stderr;
      })
      .catch((error) => console.log('error', error));
  };
  render() {
    return (
      <div>
        <Card title={'app.js'}>
          <Form.Textarea value={this.value} onChange={this.handleOnChange} />
          <Button.Success onClick={this.handleOnClick}>Run</Button.Success>
        </Card>
        <Card title={'Standard output'}>{this.output}</Card>
        <Card title={'Standard error'}>{this.error}</Card>
        <Card title={'Exit status: ' + this.exitStatus}></Card>
      </div>
    );
  }
}

ReactDOM.render(
  <div>
    <Home />
  </div>,
  document.getElementById('root')
);
